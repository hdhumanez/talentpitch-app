
# Pasos para ejecutar el proyecto

- Clonar el repositorio del backend https://gitlab.com/hdhumanez/talentpitch-api

- Usar en el backend las variables de entorno enviadas a su correo **.env**

- Pegar la carpeta **.aws** en la raiz del proyecto backend.



## Config. backend

Para ejecutar en desarrollo debe tener instalado docker desktop, ubiquese en la raiz del proyecto.

Para desarrollo:
```bash
  docker compose build
  docker compose up
```
Para producción debes descomentar el **Dockerfile** que aparece en el **docker-compose.yml**
```bash
  docker compose build
  docker compose up
```
y listo mas fácil imposible :)
## Config. App

Para ejecutar la app
- Clonar el repositorio https://gitlab.com/hdhumanez/talentpitch-app.git
- Usar las variables de entorno enviadas a su correo.

Ejecutar los comandos
```bash
  flutter doctor
  flutter pub get
  flutter run
```

**Para tener en cuenta**: 
- Indispensable tener instalado el **sdk de flutter** y toda la configuración necesaria. Para más información visite: https://docs.flutter.dev/get-started/install

- Debe estar corriendo ambos proyectos para su correcto funcionamiento.

- Debe tener un emulador corriendo o utilizar un smartphone en modo desarrollador y activa la opción de debug, como tambien tener activa la opción de permitir instalación de debugger.

## Authors

- [@hdhumanez](https://gitlab.com/hdhumanez)

