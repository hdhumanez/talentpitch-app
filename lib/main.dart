import 'dart:async';
import 'dart:developer';

import 'package:app/src/app.dart';
import 'package:app/src/core/setup/setup.dart';
import 'package:flutter/material.dart';

void main() async {
  await runZonedGuarded(() async {
    await AppSetup.init();
    runApp(const App());
  }, (err, stack) {
    log('Error: $err', name: 'main', error: err);
    log('StackTrace: $stack', name: 'main', error: stack);
  });
}
