import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/core/utils/extensions.dart';
import 'package:flutter/material.dart';
import 'package:reactive_forms/reactive_forms.dart';

class Input extends StatelessWidget {
  const Input({
    required this.formControlName,
    this.obscureText,
    this.onChanged,
    this.keyboardType,
    this.controller,
    this.initialValue,
    this.textCapitalization,
    this.textInputAction,
    this.label,
    this.errorText,
    this.style,
    this.labelStyle,
    this.floatingLabelStyle,
    this.floatingLabelBehavior,
    this.errorStyle,
    this.borderColor,
    this.prefixIcon,
    this.filled,
    this.fillColor,
    this.readOnly,
    super.key,
  });

  final void Function(FormControl<dynamic>)? onChanged;
  final bool? obscureText;
  final TextInputType? keyboardType;
  final TextEditingController? controller;
  final String? initialValue;
  final TextCapitalization? textCapitalization;
  final TextInputAction? textInputAction;
  final String? label;
  final String? errorText;
  final String formControlName;
  final TextStyle? style;
  final TextStyle? labelStyle;
  final TextStyle? floatingLabelStyle;
  final FloatingLabelBehavior? floatingLabelBehavior;
  final TextStyle? errorStyle;
  final Color? borderColor;
  final IconData? prefixIcon;
  final bool? filled;
  final Color? fillColor;
  final bool? readOnly;

  InputBorder inputBorder({
    Color? color,
  }) {
    return OutlineInputBorder(
      borderSide: BorderSide(
        color: color ?? borderColor ?? AppColors.gray,
      ),
      borderRadius: const BorderRadius.all(Radius.circular(10)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return ReactiveTextField(
      formControlName: formControlName,
      onChanged: onChanged,
      readOnly: readOnly ?? false,
      obscureText: obscureText ?? false,
      keyboardType: keyboardType,
      controller: controller,
      textCapitalization: textCapitalization ?? TextCapitalization.none,
      textInputAction: textInputAction ?? TextInputAction.next,
      style: style ??
          context.theme.textTheme.bodyLarge?.copyWith(
            color: AppColors.black,
            fontSize: 18,
            fontWeight: FontWeight.w500,
          ),
      decoration: InputDecoration(
        border: inputBorder(),
        focusedBorder: inputBorder(
          color: readOnly != null ? null : AppColors.primary,
        ),
        enabledBorder: inputBorder(),
        errorBorder: inputBorder(
          color: Colors.red,
        ),
        focusedErrorBorder: inputBorder(
          color: Colors.red,
        ),
        labelText: label,
        labelStyle: labelStyle,
        floatingLabelStyle: floatingLabelStyle,
        floatingLabelBehavior:
            floatingLabelBehavior ?? FloatingLabelBehavior.auto,
        errorText: errorText,
        errorStyle: errorStyle?.copyWith(
          color: errorStyle?.color ?? Colors.red,
          height: 1.3,
        ),
        prefixIcon: prefixIcon != null
            ? Icon(
                prefixIcon,
                color: errorText != null
                    ? errorStyle?.color
                    : style?.color ?? Colors.white,
              )
            : null,
        filled: readOnly ?? filled,
        fillColor: fillColor ??
            (readOnly != null ? AppColors.gray.withOpacity(0.2) : null),
      ),
    );
  }
}

class NormalInput extends StatelessWidget {
  const NormalInput({
    this.obscureText,
    this.onChanged,
    this.keyboardType,
    this.controller,
    this.initialValue,
    this.textCapitalization,
    this.textInputAction,
    this.label,
    this.errorText,
    this.style,
    this.labelStyle,
    this.floatingLabelStyle,
    this.floatingLabelBehavior,
    this.errorStyle,
    this.borderColor,
    this.prefixIcon,
    this.filled,
    this.fillColor,
    this.readOnly,
    super.key,
  });

  final void Function(String)? onChanged;
  final bool? obscureText;
  final TextInputType? keyboardType;
  final TextEditingController? controller;
  final String? initialValue;
  final TextCapitalization? textCapitalization;
  final TextInputAction? textInputAction;
  final String? label;
  final String? errorText;
  final TextStyle? style;
  final TextStyle? labelStyle;
  final TextStyle? floatingLabelStyle;
  final FloatingLabelBehavior? floatingLabelBehavior;
  final TextStyle? errorStyle;
  final Color? borderColor;
  final IconData? prefixIcon;
  final bool? filled;
  final Color? fillColor;
  final bool? readOnly;

  InputBorder inputBorder({
    Color? color,
  }) {
    return OutlineInputBorder(
      borderSide: BorderSide(
        color: color ?? borderColor ?? AppColors.gray,
      ),
      borderRadius: const BorderRadius.all(Radius.circular(10)),
    );
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      initialValue: initialValue,
      onChanged: onChanged,
      readOnly: readOnly ?? false,
      obscureText: obscureText ?? false,
      keyboardType: keyboardType,
      controller: controller,
      textCapitalization: textCapitalization ?? TextCapitalization.none,
      textInputAction: textInputAction ?? TextInputAction.next,
      style: style ??
          context.theme.textTheme.bodyLarge?.copyWith(
            color: AppColors.black,
            fontSize: 18,
            fontWeight: FontWeight.w500,
          ),
      decoration: InputDecoration(
        border: inputBorder(),
        focusedBorder: inputBorder(
          color: readOnly != null ? null : AppColors.primary,
        ),
        enabledBorder: inputBorder(),
        errorBorder: inputBorder(
          color: Colors.red,
        ),
        focusedErrorBorder: inputBorder(
          color: Colors.red,
        ),
        labelText: label,
        labelStyle: labelStyle,
        floatingLabelStyle: floatingLabelStyle,
        floatingLabelBehavior:
            floatingLabelBehavior ?? FloatingLabelBehavior.auto,
        errorText: errorText,
        errorStyle: errorStyle?.copyWith(
          color: errorStyle?.color ?? Colors.red,
          height: 1.3,
        ),
        prefixIcon: prefixIcon != null
            ? Icon(
                prefixIcon,
                color: errorText != null
                    ? errorStyle?.color
                    : style?.color ?? Colors.white,
              )
            : null,
        filled: readOnly ?? filled,
        fillColor: fillColor ??
            (readOnly != null ? AppColors.gray.withOpacity(0.2) : null),
      ),
    );
  }
}
