import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/core/utils/extensions.dart';
import 'package:app/src/widgets/video_card.dart';
import 'package:flutter/material.dart';

class VideoCardGroup extends StatelessWidget {
  const VideoCardGroup({
    required this.count,
    required this.physics,
    required this.shrinkWrap,
    this.padding,
    this.videosPerRow = 2,
    super.key,
  });

  final int count;
  final int videosPerRow;
  final ScrollPhysics physics;
  final EdgeInsetsGeometry? padding;
  final bool shrinkWrap;

  @override
  Widget build(BuildContext context) {
    return GridView.builder(
      shrinkWrap: shrinkWrap,
      physics: physics,
      padding: padding ?? EdgeInsets.zero,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: videosPerRow,
        childAspectRatio: 3 / 4,
        mainAxisSpacing: 10,
        crossAxisSpacing: 10,
      ),
      itemCount: count,
      itemBuilder: (BuildContext context, int index) {
        return VideoCard(
          child: Text(
            'Video ${index + 1}',
            style: context.theme.textTheme.titleLarge?.copyWith(
              color: AppColors.white,
            ),
          ),
        );
      },
    );
  }
}
