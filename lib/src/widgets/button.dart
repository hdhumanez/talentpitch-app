import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/core/utils/extensions.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Button extends StatelessWidget {
  const Button({
    required this.onPressed,
    required this.child,
    this.backgroundColor = AppColors.primary,
    this.outlineColor,
    this.loaderColor = AppColors.white,
    this.isLoading = false,
    this.width,
    this.borderRadius,
    this.small = false,
    this.disabled = false,
    this.disabledCallback,
    super.key,
  });

  factory Button.outline({
    required void Function() onPressed,
    required Widget child,
    double? width,
    bool? isLoading,
    Color? loaderColor,
    Color? backgroundColor,
    Color outlineColor = AppColors.primary,
    bool? small,
    bool? disabled,
    void Function()? disabledCallback,
  }) {
    return Button(
      small: small ?? false,
      onPressed: onPressed,
      width: width,
      backgroundColor: backgroundColor != null
          ? backgroundColor.withOpacity(0.02)
          : Colors.transparent,
      outlineColor: outlineColor,
      loaderColor: loaderColor,
      isLoading: isLoading ?? false,
      disabled: disabled ?? false,
      disabledCallback: disabledCallback,
      child: child,
    );
  }

  final void Function() onPressed;
  final Widget child;
  final Color? backgroundColor;
  final Color? loaderColor;
  final double? width;
  final bool isLoading;
  final Color? outlineColor;
  final double? borderRadius;
  final bool small;
  final bool disabled;
  final void Function()? disabledCallback;

  @override
  Widget build(BuildContext context) {
    final paddingV = 16.0 * (small ? 0.5 : 1.0);

    return DecoratedBox(
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(borderRadius ?? 10),
        border: outlineColor != null
            ? Border.all(
                color: outlineColor!,
              )
            : null,
      ),
      child: RawMaterialButton(
        elevation: 0,
        highlightElevation: 0,
        focusElevation: 0,
        hoverElevation: 0,
        onPressed: () {
          if (!isLoading && !disabled) {
            onPressed();
          }

          if (disabled && disabledCallback != null) {
            disabledCallback?.call();
          }
        },
        fillColor: disabled ? AppColors.gray : backgroundColor,
        constraints: BoxConstraints(
          minWidth: width ?? 0,
        ),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(borderRadius ?? 10),
        ),
        textStyle: context.theme.textTheme.labelLarge?.copyWith(
          color: AppColors.white,
        ),
        child: Padding(
          padding: EdgeInsets.symmetric(
            vertical: isLoading ? paddingV * 1.18 : paddingV,
            horizontal: paddingV * 2,
          ),
          child: isLoading
              ? CupertinoActivityIndicator(
                  color: loaderColor,
                )
              : child,
        ),
      ),
    );
  }
}
