import 'package:app/src/core/utils/constants.dart';
import 'package:app/src/widgets/video_card_group.dart';
import 'package:flutter/material.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({super.key});

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard>
    with AutomaticKeepAliveClientMixin {
  @override
  Widget build(BuildContext context) {
    super.build(context);
    return const VideoCardGroup(
      count: 40,
      padding: EdgeInsets.all(Constants.pagePadding),
      physics: AlwaysScrollableScrollPhysics(),
      shrinkWrap: false,
    );
  }

  @override
  bool get wantKeepAlive => true;
}
