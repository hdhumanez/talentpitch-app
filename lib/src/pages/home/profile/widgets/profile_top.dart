import 'package:app/src/core/cubits/user/cubit.dart';
import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/core/utils/constants.dart';
import 'package:app/src/core/utils/extensions.dart';
import 'package:app/src/pages/home/profile/widgets/sign_out.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProfileTop extends StatelessWidget {
  const ProfileTop({super.key});

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<UserCubit, UserState>(
      builder: (context, state) {
        if (state.user == null) {
          return const SizedBox.shrink();
        }

        return Container(
          width: context.width,
          decoration: const BoxDecoration(
            color: AppColors.primary,
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(30),
            ),
          ),
          padding: const EdgeInsets.all(Constants.pagePadding * 3),
          child: Row(
            children: [
              Expanded(
                child: Text.rich(
                  TextSpan(
                    text: 'Hola,\n',
                    style: context.theme.textTheme.titleLarge?.copyWith(
                      color: Colors.white,
                      fontWeight: FontWeight.w500,
                      height: 1,
                    ),
                    children: [
                      TextSpan(
                        text: state.user!.firstName,
                        style: context.theme.textTheme.titleLarge?.copyWith(
                          fontSize: 35,
                          color: Colors.white,
                          fontWeight: FontWeight.w700,
                          height: 1,
                        ),
                      ),
                    ],
                  ),
                  maxLines: 2,
                ),
              ),
              const SizedBox(width: 20),
              const SignOut(),
            ],
          ),
        );
      },
    );
  }
}
