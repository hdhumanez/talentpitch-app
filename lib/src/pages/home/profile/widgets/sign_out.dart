import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/core/utils/extensions.dart';
import 'package:flutter/material.dart';

class SignOut extends StatelessWidget {
  const SignOut({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: AppColors.white),
        shape: BoxShape.circle,
      ),
      child: IconButton(
        iconSize: 24,
        icon: const Icon(Icons.logout, color: AppColors.white),
        onPressed: () {
          context.logout();
        },
      ),
    );
  }
}
