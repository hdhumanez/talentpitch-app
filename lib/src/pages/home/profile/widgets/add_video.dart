import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/core/utils/constants.dart';
import 'package:app/src/core/utils/extensions.dart';
import 'package:app/src/core/utils/helpers.dart';
import 'package:app/src/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:image_picker/image_picker.dart';

class AddVideo extends StatelessWidget {
  const AddVideo({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
        top: Constants.pagePadding,
        left: Constants.pagePadding,
        right: Constants.pagePadding,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Button(
            width: context.width,
            onPressed: () {
              context.pop();
              pickVideo(source: ImageSource.gallery);
            },
            child: Text(
              'Seleccionar video de la galería',
              style: context.theme.textTheme.bodyMedium?.copyWith(
                color: AppColors.white,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          const SizedBox(height: 10),
          Button.outline(
            width: context.width,
            onPressed: () {
              context.pop();
              pickVideo(source: ImageSource.camera);
            },
            child: Text(
              'Tomar video con la cámara',
              style: context.theme.textTheme.bodyMedium?.copyWith(
                color: AppColors.primary,
                fontWeight: FontWeight.w600,
              ),
            ),
          ),
          const SizedBox(height: 30),
        ],
      ),
    );
  }
}
