import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/core/utils/constants.dart';
import 'package:app/src/core/utils/extensions.dart';
import 'package:app/src/pages/home/profile/widgets/add_video.dart';
import 'package:app/src/widgets/video_card_group.dart';
import 'package:flutter/material.dart';

class UserVideos extends StatelessWidget {
  const UserVideos({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(Constants.pagePadding),
      child: Column(
        children: [
          Row(
            children: [
              Text(
                'Mis videos',
                style: context.theme.textTheme.titleLarge?.copyWith(
                  color: AppColors.black,
                  fontWeight: FontWeight.bold,
                ),
              ),
              const Spacer(),
              CircleAvatar(
                backgroundColor: AppColors.primary,
                child: IconButton(
                  onPressed: () {
                    context.showBottomSheet<void>(
                      child: const AddVideo(),
                    );
                  },
                  icon: const Icon(Icons.add),
                  color: AppColors.white,
                ),
              ),
            ],
          ),
          const SizedBox(height: 10),
          const VideoCardGroup(
            count: 5,
            videosPerRow: 3,
            physics: NeverScrollableScrollPhysics(),
            shrinkWrap: true,
          ),
        ],
      ),
    );
  }
}
