import 'package:app/src/pages/home/profile/widgets/profile_top.dart';
import 'package:app/src/pages/home/profile/widgets/user_videos.dart';
import 'package:flutter/material.dart';

class Profile extends StatelessWidget {
  const Profile({super.key});

  @override
  Widget build(BuildContext context) {
    return const SingleChildScrollView(
      child: Column(
        children: [
          ProfileTop(),
          UserVideos(),
        ],
      ),
    );
  }
}
