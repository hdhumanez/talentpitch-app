import 'package:app/src/core/cubits/user/cubit.dart';
import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/core/utils/constants.dart';
import 'package:app/src/core/utils/extensions.dart';
import 'package:app/src/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class UserError extends StatelessWidget {
  const UserError({super.key});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(Constants.pagePadding),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              '''Ha ocurrido un error al cargar los datos del usuario, por favor, intenta de nuevo.''',
              style: context.theme.textTheme.titleLarge?.copyWith(
                color: AppColors.white,
              ),
              textAlign: TextAlign.center,
            ),
            const SizedBox(height: 20),
            Button(
              backgroundColor: AppColors.white,
              onPressed: () {
                context.read<UserCubit>().retry();
              },
              child: Text(
                'Reintentar',
                style: context.theme.textTheme.titleMedium?.copyWith(
                  color: AppColors.primary,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
