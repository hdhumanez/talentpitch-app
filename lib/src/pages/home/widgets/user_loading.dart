import 'package:app/src/core/theme/colors.dart';
import 'package:flutter/cupertino.dart';

class UserLoading extends StatelessWidget {
  const UserLoading({super.key});

  @override
  Widget build(BuildContext context) {
    return const Center(
      child: CupertinoActivityIndicator(
        color: AppColors.white,
        radius: 20,
      ),
    );
  }
}
