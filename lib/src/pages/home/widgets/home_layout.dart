import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/core/utils/extensions.dart';
import 'package:flutter/material.dart';

class HomeLayout extends StatefulWidget {
  const HomeLayout({
    required this.builder,
    super.key,
  });

  final Widget Function(BuildContext, TabController) builder;

  @override
  State<HomeLayout> createState() => _HomeLayoutState();
}

class _HomeLayoutState extends State<HomeLayout>
    with SingleTickerProviderStateMixin {
  late final TabController _tabController;
  final ValueNotifier<int> _currentIndex = ValueNotifier(0);

  @override
  void initState() {
    _tabController = TabController(
      length: 2,
      vsync: this,
    )..addListener(() {
        _currentIndex.value = _tabController.index;
      });
    super.initState();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.white,
      body: widget.builder(context, _tabController),
      appBar: AppBar(
        elevation: 0,
        backgroundColor: AppColors.primary,
        foregroundColor: AppColors.white,
        title: Image(
          image: const AssetImage(
            'assets/images/brand/logo-white.webp',
          ),
          errorBuilder: (context, error, stackTrace) {
            return Text(
              'LOGO',
              style: context.theme.textTheme.titleLarge?.copyWith(
                fontWeight: FontWeight.bold,
                color: AppColors.white,
              ),
            );
          },
        ),
      ),
      bottomNavigationBar: ValueListenableBuilder(
        valueListenable: _currentIndex,
        builder: (context, value, child) {
          return BottomNavigationBar(
            onTap: (index) {
              _tabController.animateTo(index);
            },
            currentIndex: value,
            items: const [
              BottomNavigationBarItem(icon: Icon(Icons.home), label: 'Home'),
              BottomNavigationBarItem(
                icon: Icon(Icons.settings),
                label: 'Profile',
              ),
            ],
          );
        },
      ),
    );
  }
}
