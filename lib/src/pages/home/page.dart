import 'package:app/src/core/cubits/user/cubit.dart';
import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/pages/home/dashboard/page.dart';
import 'package:app/src/pages/home/profile/page.dart';
import 'package:app/src/pages/home/widgets/home_layout.dart';
import 'package:app/src/pages/home/widgets/user_error.dart';
import 'package:app/src/pages/home/widgets/user_loading.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class Home extends StatelessWidget {
  const Home({super.key});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.primary,
      child: BlocBuilder<UserCubit, UserState>(
        builder: (context, state) {
          if (state.status == LoadingStatus.loading) {
            return const UserLoading();
          }

          if (state.status == LoadingStatus.error) {
            return const UserError();
          }

          return HomeLayout(
            builder: (context, controller) {
              return TabBarView(
                controller: controller,
                children: const [
                  Dashboard(),
                  Profile(),
                ],
              );
            },
          );
        },
      ),
    );
  }
}
