import 'package:app/src/core/cubits/auth/cubit.dart';
import 'package:app/src/pages/auth/widgets/auth_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:reactive_forms/reactive_forms.dart';

const divider = SizedBox(height: 16);

class SignUpFormFields extends StatelessWidget {
  const SignUpFormFields({super.key});

  @override
  Widget build(BuildContext context) {
    final loginCubit = context.read<AuthCubit>();

    return ReactiveForm(
      formGroup: loginCubit.signUpForm,
      child: const Column(
        children: [
          AuthInput(
            formControlName: 'first_name',
            label: 'Nombre',
            keyboardType: TextInputType.name,
          ),
          divider,
          AuthInput(
            formControlName: 'last_name',
            label: 'Apellido',
            keyboardType: TextInputType.name,
          ),
          divider,
          AuthInput(
            formControlName: 'email',
            label: 'Correo electrónico',
            keyboardType: TextInputType.emailAddress,
          ),
          divider,
          AuthInput(
            formControlName: 'password',
            label: 'Contraseña',
            isPassword: true,
          ),
        ],
      ),
    );
  }
}
