import 'dart:developer';

import 'package:app/src/core/cubits/auth/cubit.dart';
import 'package:app/src/core/cubits/user/cubit.dart';
import 'package:app/src/core/router/routes.dart';
import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/core/utils/extensions.dart';
import 'package:app/src/pages/auth/sign-up/widgets/fields.dart';
import 'package:app/src/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

class SignUpForm extends StatelessWidget {
  const SignUpForm({super.key});

  Future<void> doSignUp(BuildContext context) async {
    try {
      context.unfocus();

      final authCubit = context.read<AuthCubit>();
      final form = authCubit.signUpForm..markAllAsTouched();

      if (authCubit.state.status == AuthStatus.loading) {
        return;
      }

      if (form.valid) {
        final user = await authCubit.signUp();

        if (context.mounted) {
          context.read<UserCubit>().setUser(user);
          context.go(AppRoutes.home);
        }
      } else {
        context.showSnackBar('Completa todos los campos para crear una cuenta');
      }
    } catch (err) {
      log('Error: $err', name: '_FormSheetState.doSignUp');
      if (context.mounted) {
        context.showSnackBar(
          '''No se pudo hacer el registro, por favor verifica tus datos''',
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      mainAxisSize: MainAxisSize.min,
      children: [
        const SignUpFormFields(),
        const SizedBox(height: 16),
        BlocBuilder<AuthCubit, AuthState>(
          builder: (context, state) {
            return Button(
              backgroundColor: AppColors.white,
              loaderColor: AppColors.primary,
              onPressed: () => doSignUp(context),
              isLoading: state.status == AuthStatus.loading,
              width: context.width,
              child: Text(
                'Crear cuenta',
                style: context.theme.textTheme.bodyMedium?.copyWith(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                  color: AppColors.primary,
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}
