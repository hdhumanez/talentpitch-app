import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/core/utils/constants.dart';
import 'package:app/src/core/utils/extensions.dart';
import 'package:app/src/pages/auth/widgets/auth_layout.dart';
import 'package:app/src/pages/auth/widgets/auth_options.dart';
import 'package:flutter/material.dart';

class Auth extends StatelessWidget {
  const Auth({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primary,
      body: AuthLayout(
        child: Padding(
          padding: const EdgeInsets.all(Constants.pagePadding),
          child: Stack(
            children: [
              Center(
                child: Image(
                  image: const AssetImage(
                    'assets/images/brand/logo-white.webp',
                  ),
                  width: 200,
                  errorBuilder: (context, error, stackTrace) {
                    return Text(
                      'LOGO',
                      style: context.theme.textTheme.titleLarge?.copyWith(
                        fontWeight: FontWeight.bold,
                        color: AppColors.white,
                      ),
                    );
                  },
                ),
              ),
              const Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                child: AuthOptions(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
