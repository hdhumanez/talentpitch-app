import 'package:app/src/core/cubits/auth/cubit.dart';
import 'package:app/src/pages/auth/widgets/auth_input.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:reactive_forms/reactive_forms.dart';

class SignInFormFields extends StatelessWidget {
  const SignInFormFields({super.key});

  @override
  Widget build(BuildContext context) {
    final loginCubit = context.read<AuthCubit>();

    return ReactiveForm(
      formGroup: loginCubit.signInForm,
      child: const Column(
        children: [
          AuthInput(
            formControlName: 'email',
            label: 'Correo electrónico',
            keyboardType: TextInputType.emailAddress,
          ),
          SizedBox(height: 16),
          AuthInput(
            formControlName: 'password',
            label: 'Contraseña',
            isPassword: true,
          ),
        ],
      ),
    );
  }
}
