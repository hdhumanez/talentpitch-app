import 'dart:developer';

import 'package:app/src/core/cubits/auth/cubit.dart';
import 'package:app/src/core/cubits/user/cubit.dart';
import 'package:app/src/core/router/routes.dart';
import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/core/utils/extensions.dart';
import 'package:app/src/pages/auth/sign-in/widgets/fields.dart';
import 'package:app/src/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

class SignInForm extends StatelessWidget {
  const SignInForm({super.key});

  Future<void> doSignIn(BuildContext context) async {
    try {
      context.unfocus();

      final authCubit = context.read<AuthCubit>();
      final form = authCubit.signInForm..markAllAsTouched();

      if (authCubit.state.status == AuthStatus.loading) {
        return;
      }

      if (form.valid) {
        final user = await authCubit.signIn();

        if (context.mounted) {
          context
            ..read<UserCubit>().setUser(user)
            ..go(AppRoutes.home);
        }
      } else {
        context.showSnackBar('Completa todos los campos para iniciar sesión');
      }
    } catch (err) {
      log('Error: $err', name: '_FormSheetState.doLogin');
      if (context.mounted) {
        context.showSnackBar(
          '''No se pudo iniciar sesión, por favor verifica tus credenciales''',
        );
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      mainAxisSize: MainAxisSize.min,
      children: [
        const SignInFormFields(),
        const SizedBox(height: 50),
        BlocBuilder<AuthCubit, AuthState>(
          builder: (context, state) {
            return Button(
              backgroundColor: AppColors.white,
              loaderColor: AppColors.primary,
              onPressed: () => doSignIn(context),
              isLoading: state.status == AuthStatus.loading,
              width: context.width,
              child: Text(
                'Iniciar sesión',
                style: context.theme.textTheme.bodyMedium?.copyWith(
                  fontSize: 18,
                  fontWeight: FontWeight.w700,
                  color: AppColors.primary,
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}
