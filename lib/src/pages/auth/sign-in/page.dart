import 'package:app/src/core/router/routes.dart';
import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/core/utils/constants.dart';
import 'package:app/src/core/utils/extensions.dart';
import 'package:app/src/pages/auth/sign-in/widgets/form.dart';
import 'package:app/src/pages/auth/widgets/auth_layout.dart';
import 'package:app/src/pages/auth/widgets/auth_top_bar.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';

class SignIn extends StatelessWidget {
  const SignIn({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.primary,
      body: AuthLayout(
        appBar: const AuthTopBar(),
        child: Center(
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(Constants.pagePadding),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Center(
                    child: Image(
                      image: const AssetImage(
                        'assets/images/brand/logo-white.webp',
                      ),
                      width: 200,
                      errorBuilder: (context, error, stackTrace) {
                        return Text(
                          'LOGO',
                          style: context.theme.textTheme.titleLarge?.copyWith(
                            fontWeight: FontWeight.bold,
                            color: AppColors.white,
                          ),
                        );
                      },
                    ),
                  ),
                  const SizedBox(height: 50),
                  Text(
                    'Iniciar sesión',
                    style: context.theme.textTheme.titleLarge?.copyWith(
                      fontWeight: FontWeight.bold,
                      color: AppColors.white,
                    ),
                  ),
                  const SizedBox(height: 10),
                  Text.rich(
                    TextSpan(
                      text: '¿Nuevo en la App? ',
                      style: context.theme.textTheme.bodyMedium?.copyWith(
                        fontWeight: FontWeight.w600,
                        color: AppColors.white,
                      ),
                      children: [
                        TextSpan(
                          text: 'Regístrate gratis',
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              context.pushReplacement(
                                '${AppRoutes.auth}${AppRoutes.signUp}',
                              );
                            },
                          style: context.theme.textTheme.bodyMedium?.copyWith(
                            color: AppColors.white,
                            fontWeight: FontWeight.bold,
                            decoration: TextDecoration.underline,
                            decorationColor: AppColors.white,
                            decorationThickness: 0.5,
                          ),
                        ),
                      ],
                    ),
                  ),
                  const SizedBox(height: 20),
                  const SignInForm(),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
