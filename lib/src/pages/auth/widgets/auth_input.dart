import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/core/utils/extensions.dart';
import 'package:app/src/widgets/input.dart';
import 'package:flutter/material.dart';

class AuthInput extends StatelessWidget {
  const AuthInput({
    required this.formControlName,
    required this.label,
    this.icon,
    this.keyboardType,
    this.isPassword = false,
    super.key,
  });

  final String formControlName;
  final String label;
  final IconData? icon;
  final bool isPassword;
  final TextInputType? keyboardType;

  @override
  Widget build(BuildContext context) {
    return Input(
      formControlName: formControlName,
      label: label,
      obscureText: isPassword,
      borderColor: AppColors.black.withOpacity(0.5),
      keyboardType: keyboardType,
      fillColor: Colors.white,
      filled: true,
      style: context.theme.textTheme.bodyMedium?.copyWith(
        color: AppColors.black.withOpacity(0.8),
        fontWeight: FontWeight.w600,
      ),
      labelStyle: context.theme.textTheme.bodyMedium?.copyWith(
        color: AppColors.black.withOpacity(0.5),
      ),
      errorStyle: context.theme.textTheme.bodySmall?.copyWith(
        color: AppColors.white,
      ),
      prefixIcon: icon,
      floatingLabelBehavior: FloatingLabelBehavior.never,
    );
  }
}
