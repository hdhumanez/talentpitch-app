import 'package:app/src/core/router/routes.dart';
import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/core/utils/extensions.dart';
import 'package:app/src/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:go_router/go_router.dart';

class AuthOptions extends StatelessWidget {
  const AuthOptions({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Button(
            onPressed: () => {
              context.push('${AppRoutes.auth}${AppRoutes.signIn}'),
            },
            width: context.width * 0.42,
            backgroundColor: AppColors.white,
            loaderColor: AppColors.primary,
            child: Text(
              'Iniciar sesión',
              style: context.theme.textTheme.bodyMedium?.copyWith(
                fontSize: 18,
                fontWeight: FontWeight.w700,
                color: AppColors.primary,
              ),
            ),
          ),
        ),
        const SizedBox(width: 20),
        Expanded(
          child: Button.outline(
            onPressed: () => {
              context.push('${AppRoutes.auth}${AppRoutes.signUp}'),
            },
            width: context.width * 0.42,
            outlineColor: AppColors.white,
            loaderColor: AppColors.white,
            child: Text(
              'Registrarse',
              style: context.theme.textTheme.bodyMedium?.copyWith(
                fontSize: 18,
                fontWeight: FontWeight.w700,
                color: AppColors.white,
              ),
            ),
          ),
        ),
      ],
    );
  }
}
