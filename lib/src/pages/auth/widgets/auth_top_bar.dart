import 'package:app/src/core/theme/colors.dart';
import 'package:app/src/core/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:go_router/go_router.dart';

class AuthTopBar extends StatelessWidget implements PreferredSizeWidget {
  const AuthTopBar({this.height = kToolbarHeight, super.key});

  final double height;

  @override
  Widget build(BuildContext context) {
    return AppBar(
      elevation: 0,
      scrolledUnderElevation: 0,
      backgroundColor: Colors.transparent,
      foregroundColor: Colors.transparent,
      systemOverlayStyle: SystemUiOverlayStyle.dark,
      leadingWidth: 70,
      leading: Row(
        children: [
          const SizedBox(width: Constants.pagePadding),
          Transform.scale(
            scale: 0.8,
            child: Ink(
              decoration: const ShapeDecoration(
                color: AppColors.primary,
                shape: CircleBorder(),
              ),
              child: IconButton(
                icon: const Icon(
                  Icons.chevron_left,
                ),
                iconSize: 35,
                color: Colors.white,
                onPressed: () => context.pop(),
              ),
            ),
          ),
        ],
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height);
}
