import 'package:app/src/core/router/router.dart';
import 'package:app/src/core/setup/wrapper.dart';
import 'package:app/src/core/utils/constants.dart';
import 'package:flutter/material.dart';

class App extends StatelessWidget {
  const App({super.key});

  @override
  Widget build(BuildContext context) {
    return AppWrapper(
      child: MaterialApp.router(
        title: Constants.appName,
        debugShowCheckedModeBanner: false,
        routerConfig: AppRouter.config,
      ),
    );
  }
}
