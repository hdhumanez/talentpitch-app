import 'dart:developer';

import 'package:app/src/core/services/api.dart';
import 'package:app/src/core/setup/dependencies.dart';

abstract class Repository {
  Repository() {
    log('Initializing $runtimeType', name: 'Repository');
  }

  ApiService get apiService => getIt.get<ApiService>();
}
