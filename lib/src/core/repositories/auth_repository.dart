import 'dart:developer';

import 'package:app/src/core/repositories/repository.dart';
import 'package:app/src/core/utils/helpers.dart';

class AuthRepository extends Repository {
  Future<Map<String, dynamic>> signIn({
    required String email,
    required String password,
  }) async {
    try {
      final res = await apiService.sendRequest(
        path: '/api/login',
        method: 'POST',
        protected: false,
        data: {
          'email': email,
          'password': password,
        },
      ) as Map<String, dynamic>?;

      if (res != null && !isMapEmpty(res)) {
        return res;
      }

      throw Exception('Error signing in');
    } catch (err) {
      log('Error: $err', name: 'AuthRepository.signIn');
      return Future.error(err);
    }
  }

  Future<Map<String, dynamic>> signUp({
    required String firstName,
    required String lastName,
    required String email,
    required String password,
  }) async {
    try {
      final res = await apiService.sendRequest(
        path: '/api/signup',
        method: 'POST',
        data: {
          'first_name': firstName,
          'last_name': lastName,
          'email': email,
          'password': password,
        },
        protected: false,
      ) as Map<String, dynamic>?;

      if (res != null && !isMapEmpty(res)) {
        return res;
      }

      throw Exception('Error signing up');
    } catch (err) {
      log('Error: $err', name: 'AuthRepository.signUp');
      return Future.error(err);
    }
  }

  Future<Map<String, dynamic>> getUserData() async {
    try {
      final res = await apiService.sendRequest(
        path: '/api/users/my-data',
      ) as Map<String, dynamic>?;

      if (res != null && !isMapEmpty(res)) {
        return res;
      }

      throw Exception('Error getting user data');
    } catch (err) {
      log('Error: $err', name: 'AuthRepository.getUserData');
      return Future.error(err);
    }
  }
}
