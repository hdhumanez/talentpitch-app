import 'package:flutter/material.dart' show Color, Colors, MaterialColor;

class AppColors {
  AppColors._();
  static const int _primary = 0xFF5135C2;

  static const Color primary = Color(_primary);
  static const Color black = Color.fromARGB(255, 28, 28, 28);
  static const Color white = Color(0xFFF5F5F5);
  static const Color green = Color(0xFF35C55D);
  static final Color gray = Colors.grey.shade600;
  static final Color red = Colors.red.shade500;

  static const MaterialColor primaryMaterial = MaterialColor(
    0xFFEA80b0,
    <int, Color>{
      50: Color(_primary),
      100: Color(_primary),
      200: Color(_primary),
      300: Color(_primary),
      400: Color(_primary),
      500: Color(_primary),
      600: Color(_primary),
      700: Color(_primary),
      800: Color(_primary),
      900: Color(_primary),
    },
  );
}
