part of 'cubit.dart';

enum AuthStatus { initial, loading, success, error }

class AuthState extends Equatable {
  const AuthState({
    this.status = AuthStatus.initial,
  });

  final AuthStatus status;

  @override
  List<Object?> get props => [status];

  AuthState copyWith({
    String? email,
    String? password,
    AuthStatus? status,
  }) {
    return AuthState(
      status: status ?? this.status,
    );
  }

  @override
  bool get stringify => true;
}
