import 'dart:developer';

import 'package:app/src/core/models/user/user.dart';
import 'package:app/src/core/repositories/auth_repository.dart';
import 'package:app/src/core/setup/dependencies.dart';
import 'package:app/src/core/utils/constants.dart';
import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:reactive_forms/reactive_forms.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit() : super(const AuthState());

  final signInForm = FormGroup({
    'email': FormControl<String>(
      validators: [
        Validators.email,
        Validators.required,
      ],
    ),
    'password': FormControl<String>(
      validators: [
        Validators.required,
      ],
    ),
  });

  final signUpForm = FormGroup({
    'first_name': FormControl<String>(
      validators: [
        Validators.required,
      ],
    ),
    'last_name': FormControl<String>(
      validators: [
        Validators.required,
      ],
    ),
    'email': FormControl<String>(
      validators: [
        Validators.email,
        Validators.required,
      ],
    ),
    'password': FormControl<String>(
      validators: [
        Validators.required,
      ],
    ),
  });

  Future<User> signIn({
    Map<String, dynamic>? payload,
  }) async {
    try {
      emit(state.copyWith(status: AuthStatus.loading));

      final data = payload ??
          {
            'email': payload?['email'] ?? signInForm.value['email']! as String,
            'password':
                payload?['password'] ?? signInForm.value['password']! as String,
          };

      final res = await getIt.get<AuthRepository>().signIn(
            email: data['email']! as String,
            password: data['password']! as String,
          );

      if (res['token'] != null) {
        final prefs = getIt<SharedPreferences>();
        await prefs.setString(Constants.ssid, res['token'] as String);

        reset();
        return User.fromJson(res);
      }

      emit(state.copyWith(status: AuthStatus.error));
      throw Exception('Error al iniciar sesión');
    } catch (err) {
      emit(state.copyWith(status: AuthStatus.error));
      log('Error: $err', name: 'AuthCubit.signIn');
      return Future.error(err);
    }
  }

  Future<User> signUp() async {
    try {
      emit(state.copyWith(status: AuthStatus.loading));

      final res = await getIt.get<AuthRepository>().signUp(
            firstName: signUpForm.value['first_name']! as String,
            lastName: signUpForm.value['last_name']! as String,
            email: signUpForm.value['email']! as String,
            password: signUpForm.value['password']! as String,
          );

      if (res['ID'] != null) {
        return await signIn(
          payload: {
            'email': signUpForm.value['email']! as String,
            'password': signUpForm.value['password']! as String,
          },
        );
      }

      throw Exception('Error al registrarse');
    } catch (err) {
      emit(state.copyWith(status: AuthStatus.error));
      log('Error: $err', name: 'AuthCubit.signUp');
      rethrow;
    }
  }

  Future<void> signOut() async {
    try {
      await getIt<SharedPreferences>().remove(Constants.ssid);
      reset();
    } catch (err) {
      log('Error: $err', name: 'AuthCubit.signOut');
      rethrow;
    }
  }

  void reset() {
    signInForm.reset();
    signUpForm.reset();
    emit(const AuthState());
  }
}
