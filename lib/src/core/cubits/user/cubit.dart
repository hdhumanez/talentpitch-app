import 'dart:developer';

import 'package:app/src/core/models/user/user.dart';
import 'package:app/src/core/repositories/auth_repository.dart';
import 'package:app/src/core/setup/dependencies.dart';
import 'package:app/src/core/utils/constants.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

part 'state.dart';

class UserCubit extends Cubit<UserState> {
  UserCubit() : super(const UserState()) {
    _init();
  }

  Future<void> _init() async {
    try {
      emit(state.copyWith(status: LoadingStatus.loading));

      final prefs = getIt<SharedPreferences>();
      final authToken = prefs.getString(Constants.ssid);

      if (authToken == null) {
        throw Exception('No se encontró el token de autenticación');
      }

      final res = await getIt.get<AuthRepository>().getUserData();
      log(res.toString(), name: 'UserCubit.init');

      if (res['email'] != null) {
        final user = User.fromJson(res);
        emit(state.copyWith(user: user, status: LoadingStatus.success));
        return;
      }

      throw Exception('Error al obtener los datos del usuario');
    } catch (err) {
      log('Error: $err', name: 'UserCubit.init');
      emit(state.copyWith(status: LoadingStatus.error));
    }
  }

  void retry() {
    _init();
  }

  void setUser(User? user) {
    emit(state.copyWith(user: user));
  }

  void reset() {
    emit(const UserState());
  }
}
