part of 'cubit.dart';

enum LoadingStatus {
  initial,
  loading,
  success,
  error,
}

class UserState extends Equatable {
  const UserState({
    this.user,
    this.status = LoadingStatus.initial,
  });

  final User? user;
  final LoadingStatus status;

  @override
  List<Object?> get props => [
        user,
        status,
      ];

  UserState copyWith({
    User? user,
    LoadingStatus? status,
    bool? notificationsEnabled,
  }) {
    return UserState(
      user: user ?? this.user,
      status: status ?? this.status,
    );
  }

  @override
  bool get stringify => true;
}
