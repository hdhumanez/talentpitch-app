import 'dart:io';

import 'package:image_picker/image_picker.dart';

Future<File> pickVideo({required ImageSource source}) async {
  try {
    final picker = ImagePicker();

    final video = await picker.pickVideo(source: source);

    if (video == null) {
      throw Exception('No image selected');
    }

    // create a File from XFile
    return File(video.path);
  } catch (err) {
    return Future.error(err);
  }
}

bool isMapEmpty(Map<String, dynamic>? map) {
  if (map == null) {
    return true;
  }

  return map.isEmpty;
}
