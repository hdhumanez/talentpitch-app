class Constants {
  Constants._();

  static const String appName = 'Test App';

  static const String ssid = 'ssid';

  static const double pagePadding = 12;

  static final Map<String, String Function(Object)> validationMessages = {
    'required': (v) => 'Este campo es requerido*',
    'email': (v) => 'Ingrese un correo válido*',
  };
}
