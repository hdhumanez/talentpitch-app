import 'dart:developer';

import 'package:app/src/core/cubits/auth/cubit.dart';
import 'package:app/src/core/cubits/user/cubit.dart';
import 'package:app/src/core/router/routes.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';

extension BuildContextExtension on BuildContext {
  double get height => MediaQuery.of(this).size.height;
  double get width => MediaQuery.of(this).size.width;

  ThemeData get theme => Theme.of(this);

  void unfocus() => FocusScope.of(this).unfocus();

  void showSnackBar(
    String message, {
    Duration? duration,
  }) {
    ScaffoldMessenger.of(this)
      ..hideCurrentSnackBar()
      ..showSnackBar(
        SnackBar(
          duration: duration ?? const Duration(seconds: 3),
          content: Text(
            message,
            style: const TextStyle(
              fontSize: 17,
              fontWeight: FontWeight.w400,
            ),
            textAlign: TextAlign.center,
          ),
        ),
      );
  }

  Future<T?> showBottomSheet<T>({
    required Widget child,
    bool isScrollControlled = true,
    bool useSafeArea = true,
    Color backgroundColor = Colors.white,
  }) async {
    return showModalBottomSheet<T>(
      context: this,
      isScrollControlled: isScrollControlled,
      useSafeArea: useSafeArea,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          top: Radius.circular(10),
        ),
      ),
      builder: (_) {
        return Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            child,
          ],
        );
      },
    );
  }

  Future<void> logout() async {
    try {
      if (mounted) {
        await read<AuthCubit>().signOut();
        read<UserCubit>().reset();
        showSnackBar('Sesión cerrada correctamente.');
        go(AppRoutes.auth);
      }
    } catch (err) {
      log(err.toString(), name: 'logout');
      showSnackBar('Error al cerrar sesión. Intente nuevamente.');
    }
  }
}
