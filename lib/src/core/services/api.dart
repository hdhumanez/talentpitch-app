import 'dart:developer';

import 'package:app/src/core/setup/dependencies.dart';
import 'package:app/src/core/setup/environment.dart';
import 'package:app/src/core/utils/constants.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ApiService {
  final _dio = Dio(
    BaseOptions(
      headers: {
        'Accept': 'application/json',
        'client': 'app',
      },
      validateStatus: (status) {
        return status != null && status < 500;
      },
    ),
  );

  Future<Map<String, dynamic>> sendRequest({
    /// The path to complete the url of the request
    required String path,

    /// The HTTP method to use for the request.
    String method = 'GET',
    // Use this to send a request that is not in the api
    bool urlComplete = false,
    Map<String, dynamic>? data,
    Map<String, dynamic>? params,
    FormData? formData,
    bool protected = true,
  }) async {
    final prefs = getIt<SharedPreferences>();
    final authToken = prefs.getString(Constants.ssid);

    try {
      if (protected && authToken == null) {
        throw Exception('No token found');
      }

      final options = Options(
        method: method,
        headers: {
          'Authorization': 'Bearer $authToken',
          'Content-Type':
              formData != null ? 'multipart/form-data' : 'application/json',
        },
      );

      if (!urlComplete) {
        _dio.options.baseUrl = AppEnvironment.apiUri;
      }

      if (urlComplete && !path.startsWith(RegExp('https?:'))) {
        throw Exception('Invalid URL: $path');
      }

      if (params != null) {
        final cleanPath = params..removeWhere((key, value) => value == null);
        _dio.options.queryParameters = cleanPath;
      }

      final response = await _dio.request<Map<String, dynamic>>(
        path,
        data: formData ?? data,
        options: options,
      );

      return Map<String, dynamic>.from(response.data ?? {});
    } catch (err) {
      log(
        'ERROR: $err | METHOD:$method | PATH:$path',
        name: 'ApiService.sendRequest',
      );
      return Future.error(err);
    }
  }
}
