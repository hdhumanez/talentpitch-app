class AppRoutes {
  AppRoutes._();

  static const String auth = '/';
  static const String signIn = 'sign-in';
  static const String signUp = 'sign-up';
  static const String home = '/home';
  static const String dashboard = 'dashboard';
  static const String profile = 'profile';
}
