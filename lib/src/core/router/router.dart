import 'package:app/src/core/router/routes.dart';
import 'package:app/src/core/setup/dependencies.dart';
import 'package:app/src/core/utils/constants.dart';
import 'package:app/src/pages/auth/page.dart';
import 'package:app/src/pages/auth/sign-in/page.dart';
import 'package:app/src/pages/auth/sign-up/page.dart';
import 'package:app/src/pages/home/dashboard/page.dart';
import 'package:app/src/pages/home/page.dart';
import 'package:app/src/pages/home/profile/page.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:shared_preferences/shared_preferences.dart';

class AppRouter {
  AppRouter._();

  static final _rootNavigatorKey = GlobalKey<NavigatorState>();

  static final _router = GoRouter(
    debugLogDiagnostics: kDebugMode,
    navigatorKey: _rootNavigatorKey,
    initialLocation: AppRoutes.auth,
    routes: [
      GoRoute(
        path: AppRoutes.auth,
        name: 'auth',
        builder: (context, state) {
          return const Auth();
        },
        redirect: (context, state) {
          final hasToken =
              getIt.get<SharedPreferences>().getString(Constants.ssid) != null;

          if (hasToken) {
            return AppRoutes.home;
          }

          return null;
        },
        routes: [
          GoRoute(
            path: AppRoutes.signIn,
            name: 'sign-in',
            builder: (context, state) {
              return const SignIn();
            },
          ),
          GoRoute(
            path: AppRoutes.signUp,
            name: 'sign-up',
            builder: (context, state) {
              return const SignUp();
            },
          ),
        ],
      ),
      GoRoute(
        path: AppRoutes.home,
        name: 'home',
        builder: (context, state) {
          return const Home();
        },
        routes: [
          GoRoute(
            path: AppRoutes.dashboard,
            name: 'dashboard',
            builder: (context, state) {
              return const Dashboard();
            },
          ),
          GoRoute(
            path: AppRoutes.profile,
            name: 'profile',
            builder: (context, state) {
              return const Profile();
            },
          ),
        ],
      ),
    ],
  );

  static GoRouter get config => _router;
}
