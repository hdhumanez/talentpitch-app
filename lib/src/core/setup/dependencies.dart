import 'dart:developer';

import 'package:app/src/core/repositories/auth_repository.dart';
import 'package:app/src/core/services/api.dart';
import 'package:get_it/get_it.dart';
import 'package:shared_preferences/shared_preferences.dart';

final getIt = GetIt.instance;

class AppDependencies {
  AppDependencies._();

  static Future<void> init() async {
    log('Injecting dependencies...', name: 'AppDependencies');
    final prefs = await SharedPreferences.getInstance();

    getIt
      // Services
      ..registerSingleton<SharedPreferences>(prefs)
      ..registerSingleton<ApiService>(ApiService())
      // Repositories
      ..registerSingleton<AuthRepository>(AuthRepository());

    await getIt.allReady();
    log('Dependencies injected', name: 'AppDependencies');
  }
}
