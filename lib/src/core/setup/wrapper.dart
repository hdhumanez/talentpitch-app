import 'package:app/src/core/cubits/auth/cubit.dart';
import 'package:app/src/core/cubits/user/cubit.dart';
import 'package:app/src/core/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:reactive_forms/reactive_forms.dart';

class AppWrapper extends StatelessWidget {
  const AppWrapper({required this.child, super.key});

  final Widget child;

  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider(create: (context) => AuthCubit()),
        BlocProvider(create: (context) => UserCubit()),
      ],
      child: ReactiveFormConfig(
        validationMessages: Constants.validationMessages,
        child: child,
      ),
    );
  }
}
