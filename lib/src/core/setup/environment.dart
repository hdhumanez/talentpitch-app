import 'dart:developer';

import 'package:flutter/foundation.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:package_info_plus/package_info_plus.dart';

class AppEnvironment {
  AppEnvironment._();

  static late final String apiUri;

  static void checkInitialized() {
    assert(apiUri.isNotEmpty, 'apiUri not initialized');
  }

  static Future<void> init() async {
    final packageInfo = await PackageInfo.fromPlatform();

    const environment = kDebugMode ? 'Development' : 'Release';
    final version = packageInfo.version;
    final buildNumber = packageInfo.buildNumber;

    await dotenv.load();

    apiUri = dotenv.get('API_URI');

    checkInitialized();

    log(
      '[APP $environment: v$version+$buildNumber]',
      name: 'AppEnvironment',
    );

    log(
      'API: $apiUri',
      name: 'AppEnvironment',
    );
  }
}
