import 'dart:developer';

import 'package:app/src/core/setup/dependencies.dart';
import 'package:app/src/core/setup/environment.dart';
import 'package:app/src/core/utils/constants.dart';
import 'package:flutter/material.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

class AppSetup {
  AppSetup._();

  static Future<void> init() async {
    try {
      log('Initializing...', name: 'AppSetup');
      WidgetsFlutterBinding.ensureInitialized();

      Intl.defaultLocale = 'es';
      await initializeDateFormatting();

      await AppEnvironment.init();
      await AppDependencies.init();

      log('Running ${Constants.appName}...', name: 'AppSetup');
    } catch (err) {
      return Future.error(err);
    }
  }
}
